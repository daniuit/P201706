<?php

header("Content-type: text/html; charset=utf-8");   


function C($key='')
{
	$configs = include '../config.php';

	if($key && array_key_exists($key, $configs)){
		return $configs[$key];
	}else{
		return $configs;
	}
}  

function D($data)
{
	var_dump($data);
}

/**
 * [Ftime 实现时间转换]
 * @Author   Xuebingsi
 * @DateTime 2018-01-10T08:38:34+0800
 * @param    [int]                   $time [时间戳]
 */
function Ftime($time)
{
	$now = time();

	$diff = $now-$time;

	if($diff<60){
		return "刚刚";
	}elseif($diff<3600){
		return ceil($diff/60)."分钟前";
	}elseif($diff<86400){
		return ceil($diff/3600)."小时前";
	}elseif($diff<(86400*7)){
		return ceil($diff/86400)."天前";
	}else{
		return date("Y-m-d H:i:m",$time);
	}
}

function diffDate($date1,$date2)
{
	$dataTime1 = strtotime($date1);
	$dataTime2 = strtotime($date2);

	$diff = abs($dataTime1-$dataTime2);

	return $diff/86400;
}
/**
 * [getRand description]
 * @Author   Xuebingsi
 * @DateTime 2018-01-10T09:16:35+0800
 * @param    integer                  $num [description]
 * @return   [type]                        [description]
 */
function getRand($num=4)
{
	$str ='';

	for ($i=0; $i <$num ; $i++) { 
		$str .= mt_rand(0,9);
	}

	return $str;
}
/**
 * [getWeekDay description]
 * @Author   Xuebingsi
 * @DateTime 2018-01-10T09:11:55+0800
 * @param    string                   $date [description]
 * @return   [type]                         [description]
 */
function getWeekDay($date='')
{
	if(empty($date)){
		$time = time();
	}else{
		$time = strtotime($date);
	}

	$dateDetail = getdate($time);

	return $dateDetail['weekday'];
}

function Fdb($bit)
{
	if($bit<1024){
		return $bit."b";
	}elseif($bit<pow(1024, 2)){
		return round($bit/1024,2)."kb";
	}elseif($bit<pow(1024, 3)){
		return round($bit/pow(1024, 2),2)."mb";
	}elseif($bit<pow(1024, 4)){
		return round($bit/pow(1024, 3),2)."gb";
	}elseif($bit<pow(1024, 5)){
		return round($bit/pow(1024, 4),2)."tb";
	}
}

function del_dir($path)
{
	if(!is_dir($path)) return ;
	
	$paths = glob($path.'/*');
	// var_dump($paths);
	foreach ($paths as  $v) {
		if(is_dir($v)){
			del_dir($v);
		}else{
			// echo "unlink:$v";
			unlink($v);
		}
	}
	rmdir($path);
}


function hex2rgb($colour ) {
     if ($colour [0] == '#') {   
        $colour = substr ( $colour, 1 );   
    }   
    if (strlen ( $colour ) == 6) {   
        list ( $r, $g, $b ) = array ($colour [0] . $colour [1], $colour [2] . $colour [3], $colour [4] . $colour [5] );   
    } elseif (strlen ( $colour ) == 3) {   
        list ( $r, $g, $b ) = array ($colour [0] . $colour [0], $colour [1] . $colour [1], $colour [2] . $colour [2] );   
    } else {   
        return false;   
    }   
    $r = hexdec ( $r );   
    $g = hexdec ( $g );   
    $b = hexdec ( $b );   
    return array ('r' => $r, 'g' => $g, 'b' => $b ); 
}











 ?>