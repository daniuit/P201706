<?php 
/**
* 
*/
class Upload 
{

	public $config = [
		'rootPath'=>'./Upload',
		'maxSize'=>400000,
		'allowType'=>['jpg','png','gif']
	];//配置

	public $file;//放上传的files数据

	public $error;//错误信息

	public $suffix;//后缀
	
	/**
	 * [__construct 初始化]
	 * @Author   Xuebingsi
	 * @DateTime 2018-01-19T09:34:04+0800
	 * @param    array                    $config [description]
	 */
	public function __construct($config=[])
	{
		$this->config = array_merge($this->config,$config);

		if(empty($_FILES)){
			$this->error = "接收数据为空，可能受post限制";
			return false;
		}

		$this->file = current($_FILES);

		$this->checkRootDir();
	}
	//获取配置
	public function __get($name)
	{
		return $this->config[$name];
	}
	//检测目录
	public function checkRootDir()
	{
		$path = $this->rootPath.'/'.date("Y-m-d");
		is_dir($path) || mkdir($path,0777,true);
	}
	//外部调用 
	public function run()
	{
		$this->checkError();
		$this->checkSize();
		$this->checkType();
		return $this->moveFile();
	}
	/**
	 * [checkError 检测是否有错误]
	 * @Author   Xuebingsi
	 * @DateTime 2018-01-19T09:08:16+0800
	 * @return   [type]                   [description]
	 */
	public function checkError()
	{
		if($this->file['error']>0){
			switch ($this->file['error']) {
				case '1':
					$this->error = '文件上传失败，php.ini 中 upload_max_filesize 选项限制的值';
					break;
				case '2':
					$this->error = '上传文件的大小超过了 HTML 表单中 MAX_FILE_SIZE 选项指定的值';
					break;
				case '3':
					# code...
					break;
				default:
					# code...
					break;
			}
		}
	}

	//检测大小
	public function checkSize()
	{
		if($this->file['size']>$this->maxSize){
			$this->error = '文件上传失败，不能大于'.$this->maxSize.'字节';
		}
	}
	//检测类型
	public function checkType()
	{
		$arr = explode('.', $this->file['name']);

		$this->suffix  = end($arr);

		if(!in_array($this->suffix, $this->allowType)){
			$this->error = '文件上传失败，文件类型不允许,允许类型'.implode(',', $this->allowType);
		}
	}
	//移动文件
	public function moveFile()
	{
		if($this->error){
			return false;
		}

		$name = uniqid();

		$dest = $this->rootPath.'/'.date("Y-m-d").'/'.$name.'.'.$this->suffix;

		move_uploaded_file($this->file['tmp_name'], $dest);

		return $dest;
	}
}
















 ?>