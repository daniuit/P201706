<?php 
include './functions.php';
include './Model.class.php';
// 接删除的id
if(!isset($_SESSION['username'])){
	exit('你还没有登录');
}
$id = $_GET['id'];

//读数据库文件
$data = file_get_contents('./Db/db.json');
//把json字符串转数组
$data = json_decode($data,true);

if($data[$id]['username']==$_SESSION['username']){

	unset($data[$id]);

	//把老数据转成json字符串
	$data = json_encode($data);
	//写入数据库
	file_put_contents('./Db/db.json', $data);

	// 跳转到首页
	header("Location:./index.php");

}


 ?>
 <!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<script type="text/javascript">
	// 判断是否有错,用js弹出警告
		if(confirm("内容不是你的或者没登录")){
			location.href = "./index.php";
		}else{
			location.href = "./index.php";
		}
	</script>
</head>
<body>
	
</body>
</html>