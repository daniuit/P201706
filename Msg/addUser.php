<?php 


if(!isset($_SESSION['username'])){
	exit('你还没有登录');
}

// 引公共函数库
include './functions.php';
include './Model.class.php';

// 接收post的数据
$data = $_POST;

// 增加留言时间
$data['ctime'] = time();

$data['password'] = md5($data['password']);

unset($data['repassword']);

// 读老数据 
$oldData = file_get_contents("./Db/user.json");
//把json字符串转数组
$oldData = json_decode($oldData,true);
//把新数据追加到老数据中
$oldData[] = $data;
//把老数据转成json字符串
$oldData = json_encode($oldData);
//写入数据库
file_put_contents('./Db/user.json', $oldData);
// 跳转到首页
header("Location:./index.php");

 ?>