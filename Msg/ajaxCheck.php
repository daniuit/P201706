<?php 

switch ($_POST['key']) {
	case 'username':
		checkUsername();
		break;
	case 'phone':
		checkPhone();
		break;
	case 'email':
		checkEmail();
		break;
	case 'password':
		checkPassword();
		break;
	default:
		# code...
		break;
}

function checkUsername()
{
	$username = $_POST['value'];

	$preg = "/^[A-z][A-z0-9]{5,7}$/";

	$c = preg_match($preg, $username);

	if(!$c){
		echo json_encode(['error'=>1,'info'=>"用户名需要是6-8位"]);exit;
	}

	// 读老数据 
	$oldData = file_get_contents("./Db/user.json");
	//把json字符串转数组
	$oldData = json_decode($oldData,true);


	foreach ($oldData as  $user) {
		if($username ==$user['username']){
			echo json_encode(['error'=>1,'info'=>"用户名已存在"]);exit;
		}
	}


	echo json_encode(['error'=>0,'info'=>"用户名可用"]);
}

function checkPhone()
{
	


	echo json_encode(['error'=>0,'info'=>"手机可用"]);
}

function checkEmail()
{
	


	echo json_encode(['error'=>0,'info'=>"邮箱可用"]);
}


function checkPassword()
{
	


	echo json_encode(['error'=>0,'info'=>"密码可用"]);
}





 ?>