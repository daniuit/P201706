<?php 

//引公共函数库
include './functions.php';
//读数据库文件
$data = file_get_contents('./Db/db.json');
//把json字符串转数组
$data = json_decode($data,true);
// 排序
krsort($data);
// 总数
$count = count($data);
// 一页多少个
$row = 5;
//总页数
$page = ceil($count/$row);

$p = isset($_GET['p']) ? $_GET['p'] : 1 ;

$data = array_slice($data, ($p-1)*$row,$row,true);
// 引首页模板
include './Tpl/index.html';



?>