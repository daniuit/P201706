<?php 
if(!isset($_SESSION['username'])){
	exit('你还没有登录');
}

// 引公共函数库
include './functions.php';

// 接收post的数据
$data = $_POST;

$data['username'] = isset($_SESSION['username']) ? $_SESSION['username'] : ''; 

// 判断数据是否为空
if(empty($data['username']) || empty($data['content'])){

	$error = true;

}else{

	// 增加留言时间
	$data['ctime'] = time();

	// 读老数据 
	$oldData = file_get_contents("./Db/db.json");
	//把json字符串转数组
	$oldData = json_decode($oldData,true);
	//把新数据追加到老数据中
	$oldData[] = $data;
	//把老数据转成json字符串
	$oldData = json_encode($oldData);
	//写入数据库
	file_put_contents('./Db/db.json', $oldData);
	// 跳转到首页
	header("Location:./index.php");

}

 ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<script type="text/javascript">
	// 判断是否有错,用js弹出警告
		var  error = <?php echo $error ?>;
		if(error){

			if(confirm("用户没登录或者内容不能为空")){
				location.href = "./index.php";
			}
		}
	</script>
</head>
<body>
	
</body>
</html>