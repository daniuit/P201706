<?php
session_start();
header("Content-type: text/html; charset=utf-8");     

function D($data)
{
	var_dump($data);
}

/**
 * [Ftime 实现时间转换]
 * @Author   Xuebingsi
 * @DateTime 2018-01-10T08:38:34+0800
 * @param    [int]                   $time [时间戳]
 */
function Ftime($time)
{
	$now = time();

	$diff = $now-$time;

	if($diff<60){
		return "刚刚";
	}elseif($diff<3600){
		return ceil($diff/60)."分钟前";
	}elseif($diff<86400){
		return ceil($diff/3600)."小时前";
	}elseif($diff<(86400*7)){
		return ceil($diff/86400)."天前";
	}else{
		return date("Y-m-d H:i:m",$time);
	}
}

function diffDate($date1,$date2)
{
	$dataTime1 = strtotime($date1);
	$dataTime2 = strtotime($date2);

	$diff = abs($dataTime1-$dataTime2);

	return $diff/86400;
}
/**
 * [getRand description]
 * @Author   Xuebingsi
 * @DateTime 2018-01-10T09:16:35+0800
 * @param    integer                  $num [description]
 * @return   [type]                        [description]
 */
function getRand($num=4)
{
	$str ='';

	for ($i=0; $i <$num ; $i++) { 
		$str .= mt_rand(0,9);
	}

	return $str;
}
/**
 * [getWeekDay description]
 * @Author   Xuebingsi
 * @DateTime 2018-01-10T09:11:55+0800
 * @param    string                   $date [description]
 * @return   [type]                         [description]
 */
function getWeekDay($date='')
{
	if(empty($date)){
		$time = time();
	}else{
		$time = strtotime($date);
	}

	$dateDetail = getdate($time);

	return $dateDetail['weekday'];
}











 ?>