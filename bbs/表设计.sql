-- 用户表
create table user(
	uid int(6) primary key AUTO_INCREMENT COMMENT "主键",
	nickname char(80) not null COMMENT "昵称",
	password char(32) not null COMMENT "密码",
	email char(32) not null COMMENT "密码",
	face  char(150) not null COMMENT "头像",
	sex enum('男','女','保密') not null default "保密" COMMENT "性别",
	is_approve enum('0','1') default '0' COMMENT "认证",
	level int(1) not null default 0 COMMENT "等级",
	kiss int (5) not null COMMENT "飞吻",
	ctime int(10) unsigned not null COMMENT "创建时间",
	city char(20) not null COMMENT "城市",
	sign char(100) not null COMMENT "签名",
	openid char(32) not null default '' COMMENT "openid",
	status enum('0','1','2') not null default '0' COMMENT "0：正常，1:禁用",
	ltime int(10) unsigned not null COMMENT "登录时间",
	ip char(15) not null COMMENT "ip"
) COMMENT "用户表";


-- 问题表

create table question(
	qid int(6) primary key AUTO_INCREMENT COMMENT "主键",
	title char(100) not null COMMENT "标题",
	content text not null COMMENT "内容",
	cid int(5) unsigned not null COMMENT "分类id",
	uid int(5) unsigned not null COMMENT "用户id",
	kiss int (5) not null COMMENT "飞吻",
	status enum('0','1') default '0' COMMENT "0,未结，1已结" COMMENT "状态",

	is_top enum('0','1') default '0' COMMENT "0,普通，1置顶" COMMENT "置顶",

	is_jing enum('0','1') default '0' COMMENT "0,普通，1精华" COMMENT "精华",

	is_stop enum('0','1') default '0' COMMENT "0,普通，1禁用" COMMENT "禁用",

	ctime int(10) unsigned not null COMMENT "创建时间",
	view_num int(6) unsigned not null default 0  COMMENT "浏览量",
	answer_num int(6) unsigned not null default 0 COMMENT "回复量"
) COMMENT "问题表";

-- 回复表
create table answer(
	aid int(6) primary key AUTO_INCREMENT COMMENT "主键",
	content text not null COMMENT "内容",
	uid int(5) unsigned not null COMMENT "用户id",
	qid int(7) unsigned not null COMMENT "问题id",
	is_stop enum('0','1') default '0' COMMENT "0,普通，1采纳" COMMENT "采纳",
	zan_num int(6) unsigned not null default 0  COMMENT "点赞量",
	ctime int(10) unsigned not null COMMENT "创建时间"
) COMMENT "回复表";

-- 消息表
create table msg(
	mid int(6) primary key AUTO_INCREMENT COMMENT "主键",
	suid int(5) unsigned not null COMMENT "源用户id",
	duid int(5) unsigned not null COMMENT "目标用户id",
	qid int(7) unsigned not null COMMENT "问题id",
	type enum('0','1','2') default '0' COMMENT "0,收藏，1点赞，2回复" COMMENT "采纳",
	status enum('0','1') default '0' COMMENT "0,未读，1读过" COMMENT "状态",
	ctime int(10) unsigned not null COMMENT "创建时间"
) COMMENT "消息表";


-- 收藏表
create table collect(
	cid int(6) primary key AUTO_INCREMENT COMMENT "主键",
	uid int(5) unsigned not null COMMENT "户id",
	qid int(7) unsigned not null COMMENT "问题id",
	ctime int(10) unsigned not null COMMENT "创建时间"
) COMMENT "收藏表";

-- 分类表

create table cate(
	cid int(6) primary key AUTO_INCREMENT COMMENT "主键",
	cname int(5) unsigned not null COMMENT "户id",
	fid int(5) not null default 0 COMMENT "父id"
) COMMENT "分类表";

-- 验证表
create table vercode (
	vid int(6) primary key AUTO_INCREMENT COMMENT "主键",
	qeus char(100) not null COMMENT "问题",
	answer char(20) not null COMMENT "答案"
) COMMENT "验证表";


-- 友情链接

create table link (
	lid int(6) primary key AUTO_INCREMENT COMMENT "主键",
	lname char(100) not null COMMENT "链接名",
	url char(100) not null COMMENT "路径",
	sort int(3) not null default 999 COMMENT "排序",
	ctime int(10) unsigned not null COMMENT "创建时间"
) COMMENT "链接表";

-- 广告表

create table ad (
	aid int(6) primary key AUTO_INCREMENT COMMENT "主键",
	position int(1) not null COMMENT "广告位置",
	lname char(100) not null COMMENT "链接名",
	url char(100) not null COMMENT "路径",
	sort int(3) not null default 999 COMMENT "排序",
	ctime int(10) unsigned not null COMMENT "创建时间"
) COMMENT "广告表";

-- 签到表

create table sign (
	sid int(6) primary key AUTO_INCREMENT COMMENT "主键",
	uid int(5) unsigned not null COMMENT "户id",
	stime datetime  not null COMMENT "签到时间"
) COMMENT "签到表";

-- 点赞表

create table good(
	cid int(6) primary key AUTO_INCREMENT COMMENT "主键",
	uid int(5) unsigned not null COMMENT "户id",
	aid int(7) unsigned not null COMMENT "回复id",
	ctime int(10) unsigned not null COMMENT "创建时间"
) COMMENT "点赞表";

-- 案例表

create table demo(
	cid int(6) primary key AUTO_INCREMENT COMMENT "主键",
	uid int(5) unsigned not null COMMENT "户id",
	zan_num int(6) unsigned not null COMMENT "点赞量",
	pic char(100) not null COMMENT "图片",
	title char(100) not null COMMENT "标题",
	introduce char(100) not null COMMENT "简介",
	url char(100) not null COMMENT "路径",
	ctime int(10) unsigned not null COMMENT "创建时间"
) COMMENT "案例表";









