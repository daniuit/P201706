-- 数据库的构成
-- 库（仓库：多张表的集合）
-- 表 （货架：具体放东西货架）
-- 行 （层：货架上的层）
-- 列（字段）（抽屉：具体放东西）

-- sql语言
-- 创建库

show databases;

create database school default character set utf8;

drop database school;

use school;

show tables;

-- 先了解数据类型
-- 字符串，日期，数值，特殊 （位运算）

-- create table stu(id int(5));
-- tinyint
-- int(7)
-- char(32)
--varchar(1000)
-- text	

-- enum
-- set
-- 需求：学生表：姓名 学号 性别 年龄 出生年月 手机 地址 

create table msg(
	username char(50),
	content text,
	ctime int(10)
);

create table user(
	username char(50),
	email char(50),
	phone char(11),
	password char(32),
	ctime int(10)
);

000001
000002
000003
500056

create table stu(
	sid int(6) primary key AUTO_INCREMENT COMMENT "主键",
	name char(50) COMMENT "用户名",
	num int(6) unsigned zerofill unique COMMENT "学号",
	sex enum('男','女','人妖') default "人妖" COMMENT "性别",
	age tinyint unsigned COMMENT "年龄",
	bd date COMMENT "出生年月",
	phone char(11) not null COMMENT "手机",
	addr char(100) COMMENT "地址"
) COMMENT "学生表";


desc stu;


drop table stu;

-- 操作表

select * from stu;

-- 插入数据

insert into stu (name,age,sex) values('小明',20,'男');


insert into stu (name,age,sex) values('小林',20,'男'),('小红',18,'女');
-- 删除数据

delete from stu where name='小明';
delete from stu where sex='男';
delete from stu where age<20;

-- 更新数据

update stu set name="小红" where sid=3;

update stu set name="小红",age=30,phone='123123123' where sid=2;


CREATE TABLE `stu` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` char(20) NOT NULL DEFAULT '1' COMMENT '学生名',
  `sex` enum('男','女','保密') NOT NULL DEFAULT '保密' COMMENT '性别',
  `age` int(3) unsigned NOT NULL COMMENT '年龄',
  `qq` varchar(11) NOT NULL COMMENT 'qq',
  `score` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
)  COMMENT='学生表';


CREATE TABLE `ims_ewei_shop_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `pcate` int(11) DEFAULT '0',
  `ccate` int(11) DEFAULT '0',
  `type` tinyint(1) DEFAULT '1' COMMENT '1为实体，2为虚拟',
  `status` tinyint(1) DEFAULT '1',
  `displayorder` int(11) DEFAULT '0',
  `title` varchar(100) DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `unit` varchar(5) DEFAULT '',
  `description` varchar(1000) DEFAULT '',
  `content` text,
  `goodssn` varchar(50) DEFAULT '',
  `productsn` varchar(50) DEFAULT '',
  `productprice` decimal(10,2) DEFAULT '0.00',
  `dingjin` decimal(10,2) DEFAULT '0.00',
  `marketprice` decimal(10,2) DEFAULT '0.00',
  `costprice` decimal(10,2) DEFAULT '0.00',
  `originalprice` decimal(10,2) DEFAULT '0.00' COMMENT '原价',
  `total` int(10) DEFAULT '0',
  `totalcnf` int(11) DEFAULT '0' COMMENT '0 拍下减库存 1 付款减库存 2 永久不减',
  `sales` int(11) DEFAULT '0',
  `salesreal` int(11) DEFAULT '0',
  `spec` varchar(5000) DEFAULT '',
  `createtime` int(11) DEFAULT '0',
  `weight` decimal(10,2) DEFAULT '0.00',
  `credit` varchar(255) DEFAULT NULL,
  `maxbuy` int(11) DEFAULT '0',
  `usermaxbuy` int(11) DEFAULT '0',
  `hasoption` int(11) DEFAULT '0',
  `dispatch` int(11) DEFAULT '0',
  `thumb_url` text,
  `isnew` tinyint(1) DEFAULT '0',
  `ishot` tinyint(1) DEFAULT '0',
  `isdiscount` tinyint(1) DEFAULT '0',
  `isrecommand` tinyint(1) DEFAULT '0',
  `issendfree` tinyint(1) DEFAULT '0',
  `istime` tinyint(1) DEFAULT '0',
  `iscomment` tinyint(1) DEFAULT '0',
  `timestart` int(11) DEFAULT '0',
  `timeend` int(11) DEFAULT '0',
  `viewcount` int(11) DEFAULT '0',
  `deleted` tinyint(3) DEFAULT '0',
  `hascommission` tinyint(3) DEFAULT '0',
  `commission1_rate` decimal(10,2) DEFAULT '0.00',
  `commission1_pay` decimal(10,2) DEFAULT '0.00',
  `commission2_rate` decimal(10,2) DEFAULT '0.00',
  `commission2_pay` decimal(10,2) DEFAULT '0.00',
  `commission3_rate` decimal(10,2) DEFAULT '0.00',
  `commission3_pay` decimal(10,2) DEFAULT '0.00',
  `score` decimal(10,2) DEFAULT '0.00',
  `taobaoid` varchar(255) DEFAULT '',
  `taotaoid` varchar(255) DEFAULT '',
  `taobaourl` varchar(255) DEFAULT '',
  `updatetime` int(11) DEFAULT '0',
  `share_title` varchar(255) DEFAULT '',
  `share_icon` varchar(255) DEFAULT '',
  `cash` tinyint(3) DEFAULT '0',
  `commission_thumb` varchar(255) DEFAULT '',
  `isnodiscount` tinyint(3) DEFAULT '0',
  `showlevels` text,
  `buylevels` text,
  `showgroups` text,
  `buygroups` text,
  `isverify` tinyint(3) DEFAULT '0',
  `storeids` text,
  `noticeopenid` text,
  `tcate` int(11) DEFAULT '0',
  `noticetype` text,
  `needfollow` tinyint(3) DEFAULT '0',
  `followtip` varchar(255) DEFAULT '',
  `followurl` varchar(255) DEFAULT '',
  `deduct` decimal(10,2) DEFAULT '0.00',
  `virtual` int(11) DEFAULT '0',
  `ccates` text,
  `discounts` text,
  `nocommission` tinyint(3) DEFAULT '0',
  `hidecommission` tinyint(3) DEFAULT '0',
  `pcates` text,
  `tcates` text,
  `artid` int(11) DEFAULT '0',
  `detail_logo` varchar(255) DEFAULT '',
  `detail_shopname` varchar(255) DEFAULT '',
  `detail_btntext1` varchar(255) DEFAULT '',
  `detail_btnurl1` varchar(255) DEFAULT '',
  `detail_btntext2` varchar(255) DEFAULT '',
  `detail_btnurl2` varchar(255) DEFAULT '',
  `detail_totaltitle` varchar(255) DEFAULT '',
  `deduct2` decimal(10,2) DEFAULT '0.00',
  `ednum` int(11) DEFAULT '0',
  `edmoney` decimal(10,2) DEFAULT '0.00',
  `edareas` text,
  `cates` text,
  `saleupdate` tinyint(3) DEFAULT '0',
  `diyformtype` tinyint(3) DEFAULT '0',
  `diyformid` int(11) DEFAULT '0',
  `diymode` tinyint(3) DEFAULT '0',
  `dispatchtype` tinyint(1) DEFAULT '0',
  `dispatchid` int(11) DEFAULT '0',
  `dispatchprice` decimal(10,2) DEFAULT '0.00',
  `manydeduct` tinyint(1) DEFAULT '0',
  `shorttitle` varchar(255) DEFAULT '0',
  `leader_id` int(11) DEFAULT NULL,
  `clubgoods_status` int(11) DEFAULT NULL,
  `is_clubgoods` int(11) DEFAULT NULL,
  `club_id` int(11) DEFAULT NULL,
  `agentdata` text,
  `childprice` decimal(10,0) DEFAULT NULL,
  `member_price` decimal(10,0) DEFAULT NULL,
  `trip_xing` text,
  `trip_price` text,
  `trip_jinqun` text,
  `destination` varchar(255) DEFAULT NULL,
  `gather` varchar(255) DEFAULT NULL,
  `leader` varchar(255) DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `start_prov` varchar(30) DEFAULT NULL,
  `start_city` varchar(30) DEFAULT NULL,
  `start_desc` varchar(200) DEFAULT NULL,
  `end_prov` varchar(30) DEFAULT '广东',
  `end_city` varchar(30) DEFAULT NULL,
  `end_desc` varchar(200) DEFAULT NULL,
  `genre_xialingying` tinyint(4) DEFAULT '0',
  `genre_tuandui` tinyint(4) DEFAULT '0',
  `genre_qinzi` tinyint(4) DEFAULT '0',
  `genre_sheying` tinyint(4) DEFAULT '0',
  `genre_tubu` tinyint(4) DEFAULT '0',
  `genre_dengshan` tinyint(4) DEFAULT '0',
  `genre_luying` tinyint(4) DEFAULT '0',
  `genre_huaxue` tinyint(4) DEFAULT '0',
  `genre_caoyuan` tinyint(4) DEFAULT '0',
  `genre_yueye` tinyint(4) DEFAULT '0',
  `genre_shendu` tinyint(4) DEFAULT '0',
  `xingcheng_day` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `pricefanwei` varchar(200) DEFAULT NULL,
  `chufa` varchar(60) NOT NULL,
  `isshouye` tinyint(4) NOT NULL DEFAULT '0',
  `nandu` tinyint(4) NOT NULL DEFAULT '1',
  `chengxing` tinyint(4) NOT NULL DEFAULT '100',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_pcate` (`pcate`),
  KEY `idx_ccate` (`ccate`),
  KEY `idx_isnew` (`isnew`),
  KEY `idx_ishot` (`ishot`),
  KEY `idx_isdiscount` (`isdiscount`),
  KEY `idx_isrecommand` (`isrecommand`),
  KEY `idx_iscomment` (`iscomment`),
  KEY `idx_issendfree` (`issendfree`),
  KEY `idx_istime` (`istime`),
  KEY `idx_deleted` (`deleted`),
  KEY `idx_tcate` (`tcate`),
  FULLTEXT KEY `idx_buylevels` (`buylevels`),
  FULLTEXT KEY `idx_showgroups` (`showgroups`),
  FULLTEXT KEY `idx_buygroups` (`buygroups`)
) ENGINE=MyISAM AUTO_INCREMENT=203 DEFAULT CHARSET=utf8;



