<?php
namespace app\admin\controller;
use think\Controller;
use app\index\model\Vercode;
use think\Loader;
use think\Db;
class User extends Auth
{
    public function index()
    {
        if(input('nickname')) {
           $where['nickname'] = input('nickname'); 
        }

        $start =  input('start') ? strtotime(input('start')) : strtotime("2018-01-01");

        $end =  input('end') ? strtotime(input('end'))+86400 : time();


        $where['ctime'][] =['>',$start];


        $where['ctime'][] =['<',$end];


        $userData = Db::table('user')->where($where)->paginate(3,false,['query'=>input('')]);

        var_dump($userData);
        // 渲染模板输出
		return $this->fetch('',['userData'=>$userData,'title'=>'用户列表','data'=>input('')]);
    }

    public function welcome($value='')
    {
        // 渲染模板输出
        return $this->fetch('',['title'=>'后台首页']);
    }

    public function status()
    {
        $uid = input('uid');

        $status = Db::table('user')->where('uid',$uid)->value('status');

        if($status){
            Db::table('user')->where('uid',$uid)->update(['status'=>'0']);
        }else{
            Db::table('user')->where('uid',$uid)->update(['status'=>'1']);
        }
    }

    
}
