<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use think\Request;
class Auth extends Controller
{

	public function _initialize()
	{

		$request = Request::instance();

		$path = strtolower($request->module().'/'.$request->controller().'/'.$request->action());

		$aid = session('aid');

		// if(!$this->checkAuth($aid,$path)){
		// 	// echo "没有权限";
		// 	if(Request::instance()->isAjax()){
		// 		exit(json_encode(['error'=>1,'info'=>'你没有该权限']));
		// 	}else{
		// 		$this->error('你没有该权限',url('index/login/index'));
		// 	}
			
		// }else{
		// 	// echo "有权限";
		// }
	}

	public function checkAuth($aid,$rule)
	{
		$ruels = Db::table('auth_rule')
			->where('status','0')
			->column('rule');

		if(!in_array($rule, $ruels)){
			return true;
		}

		$rules = Db::table('auth_admin_role t1')
		->field('rules')
		->join('auth_role t2','t1.roid=t2.roid')
		->where(['aid'=>$aid,'status'=>'0'])
		->select();

		$str = '';

		foreach ($rules as  $row) {
			$str .= $row['rules'].',';
		}

		$rules = array_unique(explode(',', rtrim($str,',')));

		$res  = Db::table('auth_rule')
				->where('status','0')
				->where('rid','in',implode(',', $rules))
				->column('rule');

		if(in_array($rule, $res)){
			return true;
		}else{
			return false;
		}
	}

}