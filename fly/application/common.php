<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
/**
 * [Ftime 实现时间转换]
 * @Author   Xuebingsi
 * @DateTime 2018-01-10T08:38:34+0800
 * @param    [int]                   $time [时间戳]
 */
function Ftime($time)
{
	$now = time();

	$diff = $now-$time;

	if($diff<60){
		return "刚刚";
	}elseif($diff<3600){
		return ceil($diff/60)."分钟前";
	}elseif($diff<86400){
		return ceil($diff/3600)."小时前";
	}elseif($diff<(86400*7)){
		return ceil($diff/86400)."天前";
	}else{
		return date("Y-m-d H:i:m",$time);
	}
}

function qqLogin()
{
	require_once("../vendor/qqConnect/API/qqConnectAPI.php");
	$qc = new QC();
	$qc->qq_login();
	exit;
}

function getOpenId()
{
	require_once("../vendor/qqConnect/API/qqConnectAPI.php");
	$qc = new QC();
	$qc->qq_callback();
	return $qc->get_openid();
}

function getUserInfo()
{
	require_once("../vendor/qqConnect/API/qqConnectAPI.php");
	$qc = new QC();

	return $qc->get_user_info();
}

function sendMail($email,$title,$content)
{
	

	require '../vendor/phpmailer/PHPMailerAutoload.php';

	$mail = new PHPMailer;

	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'smtp.qq.com';  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = '113664000@qq.com';                 // SMTP username
	$mail->Password = 'famhebbbctupcajc';                           // SMTP password
	$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 465;                                    // TCP port to connect to

	$mail->setFrom('113664000@qq.com', '老司机论坛');
	$mail->addAddress($email); 

	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = $title;
	$mail->Body    = $content;

	if(!$mail->send()) {
	    echo 'Message could not be sent.';
	    echo 'Mailer Error: ' . $mail->ErrorInfo;
	} else {
	    echo 'Message has been sent';
	}
}

/**
 * 获取客户端IP地址
 * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
 * @param boolean $adv 是否进行高级模式获取（有可能被伪装） 
 * @return mixed
 */
function get_client_ip($type = 0,$adv=false) {
    $type       =  $type ? 1 : 0;
    static $ip  =   NULL;
    if ($ip !== NULL) return $ip[$type];
    if($adv){
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $arr    =   explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos    =   array_search('unknown',$arr);
            if(false !== $pos) unset($arr[$pos]);
            $ip     =   trim($arr[0]);
        }elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip     =   $_SERVER['HTTP_CLIENT_IP'];
        }elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip     =   $_SERVER['REMOTE_ADDR'];
        }
    }elseif (isset($_SERVER['REMOTE_ADDR'])) {
        $ip     =   $_SERVER['REMOTE_ADDR'];
    }
    // IP地址合法验证
    $long = sprintf("%u",ip2long($ip));
    $ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
    return $ip[$type];
}
