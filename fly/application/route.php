<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Route;

Route::rule('register','index/reg/index');
Route::rule('login','index/login/index');
Route::rule('login_check','index/login/check');
Route::rule('jie/:qid','index/jie/index');
Route::rule('u/:uid','index/home/index');

// jie/16.html

// index/jie/index/qid/19.html