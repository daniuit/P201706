<?php 
namespace app\index\model;

use think\Model;
use think\Db;

class Answer extends Model
{

	public function getAnswerForWeek()
	{

		if(!cache('AnswerForWeek')){

			$AnswerForWeek = Db::table('answer t1')
	        ->field('t2.nickname,t2.face,t2.uid, count(t1.aid) cn')
	        ->join('user t2','t1.uid=t2.uid')
	        ->where('t1.ctime','>',strtotime('-7 days'))
	        ->group('t1.uid')
	        ->order('cn desc')
	        ->limit(15)
	        ->select();


	        // "select t2.*,count(t1.aid) cn from answer t1 inner join user t2 on t1.uid=t2.uid where ctime > '43545345' group by t1.uid order by cn desc";

	        cache('AnswerForWeek',$AnswerForWeek,600);
        	return $AnswerForWeek;

		}else{
			return cache('AnswerForWeek');
		}

	}
	
}

 ?>