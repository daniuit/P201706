<?php 
namespace app\index\model;

use think\Model;
use think\Db;

class Question extends Model
{
	public function getHotQues()
	{

		if(!cache('hotQues')){
			$hotQues = Db::table('question')
	        ->field('qid,title,answer_num')
	        ->where('ctime','>',strtotime("-7 days"))
	        ->order('answer_num desc')
	        ->limit(10)
	        ->select();

	        cache('hotQues',$hotQues,600);
        	return $hotQues;

		}else{
			return cache('hotQues');
		}

	}

	public function getTopQues($value='')
	{
		# code...
	}

}

 ?>