<?php 
namespace app\index\validate;

use think\Validate;
/**
 * 用户验证器
 */
class User extends BaseValidate
{
    protected $rule = [
        'nickname'  =>  'require|length:6,8|unique:user,nickname',
        'email' =>  'email|unique:user,email',
        'password'=>'require|chsDash',
        'repassword'=>'require|confirm:password',
        'vercode'=>'checkCode'
    ];

    protected $message  =   [
    	'nickname.length'  => '昵称长度为6-8位',
    	'repassword.confirm'  => '两次密码不一致',
        'vercode.checkCode'  => '人类验证不通过',    
    ];

    protected $scene = [
        'login'  =>  ['vercode'],
        'save'=>['nickname','email'],
        'password'=>['password','repassword']
    ];

}
 ?>