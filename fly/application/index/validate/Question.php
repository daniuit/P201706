<?php 
namespace app\index\validate;

use think\Validate;
use think\Db;
/**
 * 用户验证器
 */
class Question extends BaseValidate
{
    protected $rule = [
        'cid'=>'require|number',
        'title'  =>  'require|length:18,60',
        'content' =>  'require|min:30',
        'kiss'=>'require|number|checkKiss',
        'vercode'=>'checkCode'
    ];

    protected $message  =   [
    	'title.length'  => '标题长度为6-8位',
    	'content.length'  => '内容长度为6-8位',
        'vercode.checkCode'  => '人类验证不通过', 
        'kiss.checkKiss'  => '飞吻不够',    
    ];

    public function checkKiss($kiss)
    {
        $user = Db::table('user')->find(session('uid'));

        if($user['kiss']>$kiss){
            return true;
        }else{
            return false;
        }
    }


}
 ?>