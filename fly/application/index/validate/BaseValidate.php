<?php 
namespace app\index\validate;

use think\Validate;
/**
 * 用户验证器
 */
class BaseValidate extends Validate
{
    
    /**
     * [checkCode 自定人类验证规则]
     * @Author   Xuebingsi
     * @DateTime 2018-01-30T12:28:04+0800
     * @param    [type]                   $value [description]
     * @param    [type]                   $rule  [description]
     * @param    [type]                   $data  [description]
     * @return   [type]                          [description]
     */
    public function checkCode($value,$rule,$data)
    {
    	if($value==session('answer')){
    		return true;
    	}else{
    		return false;
    	}
    }

}
 ?>