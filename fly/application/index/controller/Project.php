<?php
namespace app\index\controller;
use think\Controller;
use app\index\model\Question;
use app\index\model\Answer;
use think\Db;
use alisms\Sms;
/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-03-14 10:50:39 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-03-14 11:59:14
 */
class Project extends Auth{
	public function index()
	{

		$rootCate = Db::table('cate')->select();

		$rootCate = doCate($rootCate);

		$arr = [
			[
				'cid'=>1,
				'cname'=>'家电',
				'son'=>[
					'cid'=>2,
					'cname'=>'电视',
					'son'=>[
						'cid'=>2,
						'cname'=>'液晶电视'
					]
				]
			]
		]

		$rootCate = Db::table('cate')->where('fid',0)->select();

		$sonCate = Db::table('cate')->where('fid',$rootCate[0]['cid'])->select();


		return $this->fetch('',['title'=>'发布项目','rootCate'=>$rootCate,'sonCate'=>$sonCate]);
	}

	public function getSoncate()
	{
		$cid = input('cid');

		$sonCate = Db::table('cate')->where('fid',$cid)->select();

		return ['error'=>0,'data'=>$sonCate];

	}
}