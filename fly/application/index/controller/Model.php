<?php

/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-03-08 11:57:35 
 * @Copyright:   xuebingsi
 * @Last Modified by:   zhibinm
 * @Last Modified time: 2018-03-14 14:31:56
 */

class ClassName extends AnotherClass
{
	
	function __construct(argument)
	{
		# code...
	}

	public function run($value='')
	{
		# code...
	}

	public function test($value='')
	{
		
	}
}