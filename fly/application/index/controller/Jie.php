<?php
namespace app\index\controller;
use think\Controller;
use app\index\model\Vercode;
use think\Loader;
use think\Db;
use app\index\model\Question;
class Jie extends Auth
{
    public function index()
    {
        // 预处理
        // 加引号
        // 转义
        // 限制接受数据类型
        // 用框架本身提供的数据操作类
        // 
        $qid = input('qid');
        

    	// $qid = input('qid',null,'intval');

     //    // $qid = htmlspecialchars($qid);

     //    // $qid = intval($qid);

     //    $qid = addslashes($qid);

     //    var_dump($qid);

        $sql = "select * from user where qid=$qid";

        // var_dump($sql);

        // Db::table('think_user')->where("id=:id and username=:name")->bind(['id'=>[1,\PDO::PARAM_INT],'name'=>'thinkphp'])->select();
        // // 
        // Db::table('question')
        // ->where("qid=:qid")
        // ->bind(['qid'=>[$qid,\PDO::PARAM_INT])
        // ->select();
        
        // $res = Db::table('question')
        // ->where("qid",$qid)
        // ->select();

        // var_dump(Db::table('question')->getLastsql());

        // var_dump($res);

        // // var_dump($sql);

        // exit;

        $uid = session('uid') ? session('uid') : '';

    	Db::table('question')
        ->where('qid',$qid)
        ->setInc('view_num');
    	

    	$ques = Db::table('question t1')
    	->field('t1.*,t2.cname,t3.nickname,t3.face,t4.cid is_collect')
		->join('cate t2','t1.cid = t2.cid')
		->join('user t3','t1.uid=t3.uid')
        ->join('collect t4','t1.qid=t4.qid and t4.uid="'.$uid.'"','left')
		->where(['t1.qid'=>$qid])
		->find();

        // var_dump($ques);exit;
 
        // $ques['is_collect'] = Db::table('collect')->where(['qid'=>$qid,'uid'=>session('uid')])->find();

        $answer = Db::table('answer t1')
        ->field('t1.*,t2.nickname,t2.face,t3.gid is_zan')
        ->join('user t2','t1.uid=t2.uid')
        ->join('good t3','t1.aid=t3.aid and t3.uid="'.$uid.'"','left')
        ->where(['qid'=>$qid])
        ->select();


        $hotQues = (new Question())->getHotQues();

    	return $this->fetch('',['ques'=>$ques,'answer'=>$answer,'hotQues'=>$hotQues,'title'=>$ques['title']]);
    }

    public function add()
    {
        
    	$cates = Db::table('cate')->select();

    	$ques = (new Vercode())->getRandOne();
         // 渲染模板输出
		return $this->fetch('',['ques'=>$ques,'cates'=>$cates,'title'=>"发贴"]);
    }

    public function addPost()
    {
    	$data = input('post.');

    	$validate = Loader::validate('question');

		if(!$validate->check($data)){

			return ['error'=>1,'info'=>$validate->getError()];
		}

		if($data['cid']!=1){
			unset($data['ext']);
		}else{
			$data['ext'] = json_encode($data['ext']);
		}


		$data['ctime'] = time();

		$data['uid'] = session('uid');


        Db::startTrans();
        
        try{

            $res = Db::table('question')->insert($data);

            if($res){

                $url = url('index/jie/index',['qid'=>Db::table('question')->getLastInsID()]);


                Db::table('user')->where('uid', session('uid'))->setDec('kiss',$data['kiss'] );

                Db::commit();

                return ['error'=>0,'info'=>'发布成功','url'=>$url];
            }else{
                return ['error'=>1,'info'=>'发布失败'];
            }

                
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();

            return ['error'=>1,'info'=>'发布失败'];
        }


		

    }

    public function upload()
    {
        // var_dump($_FILES);exit;
         // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file('file');

        if($file){

            $info = $file->validate(['size'=>150678,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads/imgs');

            if($info){

                $faceUrl = config('root_path').'uploads/imgs/'.date('Ymd').'/'.$info->getFilename();

                return ['code'=>0,'msg'=>'上传成功','data'=>['src'=>$faceUrl]];

            }else{
                return ['code'=>1,'msg'=>$file->getError()];
            }
        }
    }
    public function replay()
    {
       $data = input('post.');
       $data['uid'] = session('uid');
       $data['ctime'] = time();

       $res = Db::table('answer')->insert($data);

       Db::table('question')->where('qid',$data['qid'])->setInc('answer_num');

       if($res){
            return ['error'=>0,'info'=>'回复成功'];
        }else{
            return ['error'=>1,'info'=>'回复失败'];
        }

    }

    public function collect()
    {
        $data = input('post.');

        $data['uid'] = session('uid');

        $res = Db::table('collect')->where($data)->find();

        if($res){

            Db::table('collect')->where($data)->delete();

            return ['error'=>2,'info'=>'取消收藏'];

        }else{

            $data['ctime'] =time();

            Db::table('collect')->insert($data);

            return ['error'=>0,'info'=>'收藏成功'];

        }
    }

    public function zan()
    {
        $data = input('post.');

        $data['uid'] = session('uid');

        $res = Db::table('good')->where($data)->find();

        if($res){

            Db::table('good')->where($data)->delete();

            Db::table('answer')->where('aid',$data['aid'])->setDec('zan_num');

            return ['error'=>1,'info'=>'取消点赞'];

        }else{

            $data['ctime'] =time();

            Db::table('good')->insert($data);

            Db::table('answer')->where('aid',$data['aid'])->setInc('zan_num');

            return ['error'=>0,'info'=>'点赞成功'];

        }
    }
}
