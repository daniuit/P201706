<?php
namespace app\index\controller;
use think\Controller;
use app\index\model\Vercode;
use think\Loader;
use think\Db;
use gt\Gt;
use think\Request;
class Login extends Auth
{
    public function index()
    {
        
    	$ques = (new Vercode())->getRandOne();
        // 渲染模板输出
		return $this->fetch('',['ques'=>$ques,'title'=>'登录']);
    }

    public function start()
    {
        if(Request::instance()->isAjax()){
            echo  Gt::start(); 
        }else{
            return '非法请求';
        }
    }

    public function check()
    {
    	$data = input('post.');

        if(!Gt::logincheck()){
            return ['error'=>1,'info'=>"极验证不通过"];
        }

        echo 99999;exit;

    	$validate = Loader::validate('User');

		if(!$validate->scene('login')->check($data)){
			return ['error'=>1,'info'=>$validate->getError()];
		}

		unset($data['vercode']);

		$data['password'] = md5($data['password']);

		$res = Db::table('user')->where($data)->find();

		if($res){
            if($res['status']==1){
                return ['error'=>1,'info'=>'帐号被禁用，请联系110@qq.com开通'];
            }
			session('uid',$res['uid']);
			session('nickname',$res['nickname']);
			session('face',$res['face']);

			return ['error'=>0,'info'=>'登录成功'];
		}else{
			return ['error'=>1,'info'=>'邮箱或者密码错误'];
		}


    }

    public function loginout()
    {
    	session(null);
    	$this->success('退出成功', 'index/index/index');
    }

    public function qqlogin()
    {
    	qqLogin();
    }

    public function qqreturn()
    {
    	
    	$openid = getOpenId();

    	$res = Db::table('user')->where('openid',$openid)->find();

        if($res){
            session('uid',$res['uid']);
            session('nickname',$res['nickname']);
            session('face',$res['face']);
            $this->success('登录成功',url('index/index/index'));
        }else{
            
            $userInfo = getUserInfo();

            $data['nickname'] = $userInfo['nickname'];
            $data['sex'] = $userInfo['gender'];
            $data['city'] = $userInfo['city'];
            $data['face'] = $this->getqqface($userInfo['figureurl_2']);
            $data['openid'] = $openid;
            $data['ctime'] = time();

            $res = Db::table('user')->insert($data);
            
            session('uid',Db::table('question')->getLastInsID());
            session('nickname',$data['nickname']);
            session('face',$data['face']);
            $this->success('登录成功',url('index/index/index'));
        }
    }
    public function getqqface($face)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $face);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $output = curl_exec($ch);

        curl_close($ch); 

        $face = uniqid().'.png';

        file_put_contents('./qqface/'.$face, $output);

        return 'qqface/'.$face;


    }
}
