<?php
namespace app\index\controller;
use think\Controller;
use think\Db;
use think\Loader;
class Set extends Auth
{
    public function index()
    {
    	$uid = session('uid');

    	$user = Db::table('user')->find($uid);

        $user['title']='个人中心设置';

        return $this->fetch('',$user);
    }

    public function save()
    {
        return $this->checkdata('save');
    }
    public function password()
    {
        return $this->checkdata('password');
    }

    public function checkdata($scene)
    {
        $data = input('post.');

        $data['uid'] = session('uid');

        $validate = Loader::validate('User');

        if(!$validate->scene($scene)->check($data)){

            return ['error'=>1,'info'=>$validate->getError()];
        }

        $res = Db::table('user')->update($data);


        if($res){
            return ['error'=>0,'info'=>'修改成功'];
        }else{
            return ['error'=>0,'info'=>'数据未变更'];
        }
    }

    public function upload()
    {
        // var_dump($_FILES);exit;
         // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file('file');

        if($file){

            $info = $file->validate(['size'=>150678,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads');

            if($info){

                $faceUrl = 'uploads/'.date('Ymd').'/'.$info->getFilename();

                Db::table('user')->update(['face'=>$faceUrl,'uid'=>session('uid')]);

                session('face',$faceUrl);

                $meitu = input('get.meitu');

                if($meitu){
                    echo "0";
                }else{
                    return ['code'=>0,'msg'=>'上传成功','data'=>['src'=>$faceUrl]];
                }
            }else{
                return ['code'=>1,'msg'=>$file->getError()];
            }
        }
    }
}