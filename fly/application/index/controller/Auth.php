<?php
namespace app\index\controller;
use think\Controller;
use app\index\model\Question;
use think\Db;
use think\Request;
class Auth extends Controller
{

	public function _initialize()
	{
		$auth_rules = config('auth_rule');

		$request = Request::instance();

		$path = strtolower($request->module().'/'.$request->controller().'/'.$request->action());

		if(in_array($path, $auth_rules)){
			if(!session('uid')){
				if(Request::instance()->isAjax()){
					exit(json_encode(['error'=>1,'info'=>'请先登录']));
				}else{
					$this->error('你需要先登录',url('index/login/index'));
				}
				
			}
		}
	}

}