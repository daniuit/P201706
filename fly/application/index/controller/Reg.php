<?php
namespace app\index\controller;
use think\Controller;
use app\index\model\Vercode;
use \think\Validate;
use think\Loader;
use think\Db;
/**
 * 注册控制器
 */
class Reg extends Auth
{	
	/**
	 * [index 注册首页]
	 * @Author   Xuebingsi
	 * @DateTime 2018-01-30T12:26:56+0800
	 * @return   [type]                   [description]
	 */
    public function index()
    {
    	$ques = (new Vercode())->getRandOne();
         // 渲染模板输出
		return $this->fetch('',['ques'=>$ques,'title'=>'注册']);
    }
    /**
     * [add 注册数据增加]
     * @Author   Xuebingsi
     * @DateTime 2018-01-30T12:27:07+0800
     */
    public function add()
    {
    	$data = input('post.');
    	
    	$validate = Loader::validate('User');

		if(!$validate->check($data)){

			return ['error'=>1,'info'=>$validate->getError()];
		}

		$data['password'] = md5($data['password']);

		$data['ctime'] = time();

		$res = Db::table('user')->insert($data);

		if($res){
			return ['error'=>0,'info'=>'注册成功'];
		}else{
			return ['error'=>1,'info'=>'注册失败'];
		}
    }
}
