<?php
namespace app\index\controller;
use think\Controller;
use think\Db;
class Sign extends Auth
{
    public function add()
    {
        $data = Db::table('sign')->where(['uid'=>session('uid'),'date'=>date('Y-m-d')])->find();

        if($data){
        	return ['error'=>1,'info'=>'请不要重复签到'];
        }else{


        	$yesterday = Db::table('sign')->where(['uid'=>session('uid'),'date'=>date('Y-m-d',strtotime('-1 days'))])->find();

        	if($yesterday){
        		$insertData['cn'] = $yesterday['cn']+1;
        	}else{
        		$insertData['cn'] = 1;
        	}

        	$insertData['uid'] = session('uid');
        	$insertData['stime'] = time();
        	$insertData['date']=date('Y-m-d');
        	$res = Db::table('sign')->insert($insertData);


        	if($res){
				return ['error'=>0,'info'=>'签到成功'];
			}else{
				return ['error'=>1,'info'=>'签到失败'];
			}
        }
    }
}