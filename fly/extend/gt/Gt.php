<?php
namespace gt;
/**
 * @Author: zhibinm (113664000@qq.com)
 * @Date:   2018-03-13 10:11:23 
 * @Copyright:   xuebingsi
 * @Last Modified by:   Zhibinm
 * @Last Modified time: 2018-03-13 10:49:10
 */
/**
* 
*/
class Gt 
{
	
	public static function start()
	{
		

		$GtSdk = new GeetestLib(config('gt.CAPTCHA_ID'), config('gt.PRIVATE_KEY'));

		$data = array(
				"user_id" => "888", # 网站用户id
				"client_type" => "web", #web:电脑上的浏览器；h5:手机上的浏览器，包括移动应用内完全内置的web_view；native：通过原生SDK植入APP应用的方式
				"ip_address" => "127.0.0.1" # 请在此处传输用户请求验证时所携带的IP
		);

		$status = $GtSdk->pre_process($data, 1);
		session('gtserver',$status);
		session('user_id',$data['user_id']);
		return $GtSdk->get_response_str();
	}

	public static function logincheck()
	{
		

		$GtSdk = new GeetestLib(config('gt.CAPTCHA_ID'), config('gt.PRIVATE_KEY'));


		$data = array(
	        "user_id" => '99999', # 网站用户id
	        "client_type" => "web", #web:电脑上的浏览器；h5:手机上的浏览器，包括移动应用内完全内置的web_view；native：通过原生SDK植入APP应用的方式
	        "ip_address" => "127.0.0.1" # 请在此处传输用户请求验证时所携带的IP
	    );
		if (session('gtserver') == 1) {
		   //服务器正常
		    $result = $GtSdk->success_validate($_POST['geetest_challenge'], $_POST['geetest_validate'], $_POST['geetest_seccode'], $data);
		    if ($result) {
		        return true;
		    } else{
		        return false;
		    }
		}else{  //服务器宕机,走failback模式
		    if ($GtSdk->fail_validate($_POST['geetest_challenge'],$_POST['geetest_validate'],$_POST['geetest_seccode'])) {
		        return true;
		    }else{
		        return false;
		    }
		}
	}
}