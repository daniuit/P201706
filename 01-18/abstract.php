<?php 

// 有抽象方法的类叫抽象类，没有具体内容方法（空方法）抽象 方法

abstract class	father {

	abstract  public function run();
	abstract  public function show();

	public function abc($value='')
	{
		# code...
	}
}

/**
* 
*/
class A extends father
{
	
	public function run()
	{
		$this->abc();
	}

	public function show()
	{
		# code...
	}
}

class B extends father
{
	
	public function run()
	{
		# code...
	}

	public function show()
	{
		# code...
	}
}

$obj = new B();

























 ?>