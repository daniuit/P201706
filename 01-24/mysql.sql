-- 帖子表：主键，标题，内容，分类，创建时间，浏览量，回复量，状态（删禁？）
-- 用户表：主键，姓名，密码，性别，头像，创建时间，登录时间，登录ip，积分，城市，个性签名，等级，状态
-- 回复表：主键，内容，回复人，回复问题，点赞数，创建时间，回复时间。


create table question(
	qid int(6) primary key AUTO_INCREMENT COMMENT "主键",
	title char(100) not null COMMENT "标题",
	content text not null COMMENT "内容",
	cid int(5) unsigned not null COMMENT "分类id",
	ctime int(10) unsigned not null COMMENT "创建时间",
	view_num int(6) unsigned not null default 0  COMMENT "浏览量",
	answer_num int(6) unsigned not null default 0 COMMENT "回复量",
	status enum('0','1','2') not null default '0' COMMENT "0：正常，1:审核中，2：未通过"
) COMMENT "帖子表";
-- 用户表：主键，姓名，密码，性别，头像，创建时间，登录时间，登录ip，积分，城市，个性签名，等级，状态

create table user(
	uid int(6) primary key AUTO_INCREMENT COMMENT "主键",
	nickname char(80) not null COMMENT "姓名",
	password char(32) not null COMMENT "密码",
	sex enum('男','女','保密') not null default "保密" COMMENT "性别",
	face  char(150) not null COMMENT "头像",
	ctime int(10) unsigned not null COMMENT "创建时间",
	ltime int(10) unsigned not null COMMENT "登录时间",
	ip char(15) not null COMMENT "ip",
	score int (5) not null COMMENT "积分",
	city char(20) not null COMMENT "城市",
	sign char(100) not null COMMENT "签名",
	level int(1) not null COMMENT "等级",
	status enum('0','1','2') not null default '0' COMMENT "0：正常，1:禁用"
) COMMENT "用户表";
-- 回复表：主键，内容，回复人，回复问题，点赞数，创建时间，回复时间。

create table answer(
	aid int(6) primary key AUTO_INCREMENT COMMENT "主键",
	content text not null COMMENT "内容",
	uid int(5) unsigned not null COMMENT "用户id",
	qid int(7) unsigned not null COMMENT "问题id",
	zan_num int(6) unsigned not null default 0  COMMENT "点赞量",
	ctime int(10) unsigned not null COMMENT "创建时间"
) COMMENT "回复表";



insert into user (nickname,age,status) values('小明',18,'2');

delete from user where sex='男';

update user set name="sdfsdf",sex="用户表" where id=90;


select * from user;


-- 查询 

select * from stu;

select name,age,sex,phone from stu;

select name,age as a from stu;

select name,age a from stu;

select * from stu where age>20 and sid>4 and name='小明';

select * from stu where age<>20;

select DISTINCT age from stu;


select * from stu order by age ;

select * from stu order by age desc;

select * from stu order by age,phone;

select * from stu where age BETWEEN  10 and 20;

select * from stu where age in (20,30,40,50,60);


select * from stu where name like "%红%";



select *,if(age>20,'中年','小孩') nilinchen from stu;


select * from stu order by rand() limit 3;


insert into stu (username,bd) values('小马',now());