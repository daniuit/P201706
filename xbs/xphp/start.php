<?php 

define('__PUBLIC__', './public');

define('FRAME_PATH', './xphp');

define('COMMON_PATH',FRAME_PATH.'/common' );
define('CORE_PATH',FRAME_PATH.'/core' );

define('LIB_PATH',FRAME_PATH.'/lib' );

define('VENDOR_PATH',FRAME_PATH.'/vendor' );

require COMMON_PATH.'/functions.php';


$module = isset($_GET['m']) ? strtolower($_GET['m']) :C('DEFALUT_MODULE');

$controller = isset($_GET['c']) ? ucfirst($_GET['c']) :C('DEFALUT_CONTROLLER');

$action = isset($_GET['a']) ? $_GET['a'] :C('DEFALUT_ACTION');

// var_dump($module);
// var_dump($controller);
// var_dump($action);

define('MODULE_NAME',$module);
define('CONTROLLER_NAME',$controller);
define('ACTION_NAME',$action);

define('CONTROLLER_PATH', APP_PATH.'/'.$module.'/controller');
define('VIEW_PATH', APP_PATH.'/'.$module.'/view');

define('MODEL_PATH', APP_PATH.'/'.$module.'/model');

function __autoload($classname)
{
	$paths = [
		CONTROLLER_PATH.'/'.$classname.'.class.php',
		MODEL_PATH.'/'.$classname.'.class.php',
		CORE_PATH.'/'.$classname.'.class.php'
	];

	foreach ($paths as $path) {
		if(file_exists($path)){
			include $path;
			return;
		}
	}
}

$controller = $controller."Controller";

$obj = new $controller();

$obj->$action();

// var_dump(CONTROLLER_PATH);
// var_dump(VIEW_PATH);
// var_dump(MODEL_PATH);























 ?>