<?php 
/**
* 验证码
*/
class Code 
{	
	public $config = [
		'width'=>200,
		'height'=>100,
		'fontsize'=>50,
		'length'=>4,
		'interfere'=>0,
		'str'=>"1234567890qwertyuiopasdfghjklzxcvnmQWERTYUIOPASDFGHJKLZXCVBNMN",
		'bgColor'=>"#fff"
	];

	public $src;//存储画布
	
	//初始化
	public function __construct($config=[])
	{
		$this->config = array_merge($this->config,$config);
	}

	public function __get($name)
	{
		return $this->config[$name];
	}

	public function entry()
	{
		$this->createImg();

		$this->interfere();

		$this->addText();

		header("Content-type: image/png");

		imagepng($this->src);  

		imageDestroy($this->src);
	}

	public function interfere()
	{
		if($this->interfere){
			for ($i=0; $i <$this->interfere ; $i++) { 

				$color = imageColorAllocate($this->src,mt_rand(100,255),mt_rand(200,255),mt_rand(200,255));

				$str = $this->str[mt_rand(0,strlen($this->str)-1)];

				imagettftext ( $this->src, 20 , mt_rand(-15,15) , mt_rand(0,200) , mt_rand(0,100) , $color , './zhongwen.ttf' , $str);



			}
		}
	}

	public function createImg()
	{

		$this->width = $this->fontsize*$this->length >$this->width ? $this->fontsize*$this->length : $this->width;

		$this->src = imageCreateTrueColor($this->width,$this->height);

		$rbg = hex2rgb($this->bgColor);

		$color = imageColorAllocate($this->src,$rbg['r'],$rbg['g'],$rbg['b']); 

		imageFill($this->src,0,0,$color);
		
	}

	public function addText()
	{
		$code = '';
		for ($i=0; $i <$this->length ; $i++) { 

			$color = imageColorAllocate($this->src,mt_rand(0,255),mt_rand(0,255),mt_rand(0,100));

			$str = $this->str[mt_rand(0,strlen($this->str)-1)];

			$code .=$str;



			$fontsize = $this->fontsize > $this->height*0.8 ? $this->height*0.8 : $this->fontsize ;

			$diff = ($this->width-$fontsize*$this->length)/($this->length+1);

			$x = $diff+$i*($fontsize+$diff);

			$y = ($this->height+$fontsize)/2;

			imagettftext ( $this->src, $fontsize , mt_rand(-15,15) , $x , $y , $color , './zhongwen.ttf' , $str);
		}

		return $code;
	}


}
















 ?>