<?php 
/**
* 数据操作类
*/
class Model 
{
	public $table;
	public static $db;
	public $error;
	public $primaryKey;
	public $where;
	public $limit;
	public $order;
	/**
	 * [__construct 连接数据库]
	 * @Author   Xuebingsi
	 * @DateTime 2018-01-27T09:27:02+0800
	 * @param    string                   $table [description]
	 */
	function __construct($table='')
	{
		$this->table= $table;

		try {
			if(!self::$db){
				self::$db = @new PDO(C('DB_TYPE').":host=".C('DB_HOST').";dbname=".C('DB_NAME').";charset=utf8",C('DB_USER'),C('DB_PASS'));
			}

			if(!empty($table)){
				$this->getPrimaryKey();
			}
		} catch (PDOException $e) {
			$this->error = $e->getMessage();
		}
	}

	public function find($id='')
	{
		if($this->primaryKey && $id){

			$sql = "select * from ".$this->table." where ".$this->primaryKey."='{$id}'";

			return  current($this->query($sql));

		}else if($this->where){
			$sql = "select * from ".$this->table.$this->where;

			return  current($this->query($sql));

		}else{
			$this->error = "使用find函数需要对应的表有主键";
			return false;
		}
		
	}

	public function getPrimaryKey()
	{
		$sql = "desc ".$this->table;

		$field = $this->query($sql);

		foreach ($field as $row) {
			if($row['Key']=='PRI'){
				$this->primaryKey = $row['Field'];
				return;
			}
		}
	}

	public function del($id)
	{
		if($this->primaryKey){

			if(is_array($id)){
				$sql = "delete from ".$this->table." where ".$this->primaryKey." in(".implode(',', $id).")";
			}else{
				$sql = "delete from ".$this->table." where ".$this->primaryKey."='{$id}'";
			}

			return  $this->exec($sql);
		}else{
			$this->error = "使用del函数需要对应的表有主键";
			return false;
		}
	}

	public function select()
	{
		$sql = "select * from ".$this->table.$this->where.$this->order.$this->limit;

		var_dump($sql);

		return $this->query($sql);
	}

	public function where($where)
	{
		$this->where = " where ";
		foreach ($where as $k => $v) {
			$this->where .=" ".$k."='".$v."' and";
		}
		$this->where = rtrim($this->where,'and');
		return $this;
	}

	public function limit($limit)
	{
		$this->limit = " limit ".$limit;
		return $this;
	}

	public function order($order)
	{
		$this->order = " order by ".$order;
		return $this;
	}
	/**
	 * [query 执行原生]
	 * @Author   Xuebingsi
	 * @DateTime 2018-01-27T09:30:07+0800
	 * @param    [type]                   $sql [description]
	 * @return   [type]                        [description]
	 */
	public function query($sql)
	{
		$resObj = self::$db->query($sql);

		if(!$resObj){
			$this->error = self::$db->errorInfo();
			return false;
		}else{
			return $resObj->fetchAll(PDO::FETCH_ASSOC);
		}
	}

	public function exec($sql)
	{
		$res = self::$db->exec($sql);

		if($res){
			return true;
		}else{
			$this->error = self::$db->errorInfo();
			return false;
		}
	}

	public function add($data=array())
	{
		if(is_array($data)){

			var_dump($data);

			$field = implode(',', array_keys($data));

			$values = implode("','", $data);

			$sql = "insert into ".$this->table."(".$field.") values('".$values."')";

			var_dump($sql);

			return $this->exec($sql);
		}else{
			$this->error = "使用add函数需要传对应原数组";
			return false;
		}
	}

	public function update($data)
	{
		if(is_array($data) && $this->where){

			$str = '';

			foreach ($data as $k => $v) {
				$str .= $k."='".$v."',";
			}

			$str = rtrim($str,',');

			$sql = "update ".$this->table." set ".$str.$this->where;

			var_dump($sql);

			return $this->exec($sql);
		}else{
			$this->error = "使用update函数需要传对应原数组并且要有条件";
			return false;
		}
	}
}















 ?>